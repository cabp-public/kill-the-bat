const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
  template: './src/index.html',
  filename: './index.html'
});

const copyPlugin = new CopyWebpackPlugin([
  { from: './src/assets/exportables/gotham.jpg', to: './assets/' },
  { from: './src/assets/exportables/card_batman.png', to: './assets/' },
  { from: './src/assets/exportables/card_joker.png', to: './assets/' },
  { from: './src/assets/exportables/card_catwoman.png', to: './assets/' },
  { from: './src/assets/exportables/card_riddler.png', to: './assets/' },
  { from: './src/assets/exportables/card_harvey.png', to: './assets/' },
  { from: './src/assets/exportables/knob_bet.png', to: './assets/' },
  { from: './src/assets/exportables/knob_go.png', to: './assets/' },
  { from: './src/assets/exportables/control_bet_plus.png', to: './assets/' },
  { from: './src/assets/exportables/control_bet_triangle.png', to: './assets/' },
  { from: './src/assets/exportables/control_bet_minus.png', to: './assets/' },
  { from: './src/assets/exportables/control_top_right.png', to: './assets/' },
  { from: './src/assets/exportables/control_bottom_left.png', to: './assets/' },
  { from: './src/assets/exportables/control_play.png', to: './assets/' },
  { from: './src/assets/exportables/balance.png', to: './assets/' },
  { from: './src/assets/exportables/audio/partyman.ogg', to: './assets/' },
  { from: './src/assets/exportables/audio/partyman.mp3', to: './assets/' }
]);

const writePlugin = new WriteFilePlugin({
  test: /\.(jpg|ico|mp3)$/,
  useHashIndex: false
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: 'url-loader'
        },
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        use: {
          loader: 'file-loader'
        },
      }
    ]
  },
  plugins: [htmlPlugin, copyPlugin, writePlugin]
};