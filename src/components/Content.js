import React, { Component } from 'react';
import { connect } from 'react-redux';
import Preloader from './preloader/Preloader';
import GameContainer from './GameContainer';
import PlayerAvatar from './PlayerAvatar';
import PlayerRegisterModal from './PlayerRegisterModal';
import * as M from 'materialize-css';
import KillTheBat from './../KillTheBat';

const _defaults = {
  STORE_KEYS: {
    toastValue: 'TOAST_VALUE'
  }
};

class Content extends Component {
  constructor(props) {
    super(props);

    this.state = { toastOn: false };

    // Bindings
    this.triggerToast = this.triggerToast.bind(this);
    this.onToastDismissed = this.onToastDismissed.bind(this);
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
    const { TOAST_VALUE } = nextProps;
    const { toastOn } = this.state;

    if ( TOAST_VALUE && !toastOn ) {
      this.setState({ toastOn: true });
      this.triggerToast(TOAST_VALUE);
    }

    if ( !TOAST_VALUE && toastOn ) {
      this.setState({ toastOn: false });
      M.Toast.dismissAll();
    }
  }

  triggerToast(params) {
    M.toast({
      html: params.html,
      displayLength: 2000,
      classes: 'rounded ' + params.style,
      completeCallback: this.onToastDismissed
    });
  }

  onToastDismissed() {
    KillTheBat.StoreManager.setKey({
      type: _defaults.STORE_KEYS.toastValue,
      value: false
    });
  }

  render() {
    const { LOADER_ACTIVE, PLAYER_DATA } = this.props;
    const canPlay = !LOADER_ACTIVE && PLAYER_DATA;

    return (
      <div>
        { LOADER_ACTIVE ? <Preloader /> : null }

        <GameContainer />
        <PlayerRegisterModal />
      </div>
    )
  }
}

const mapStateToProps = (state) => KillTheBat.StoreManager.mapStateToProps('Content', state);
export default connect(mapStateToProps, {})(Content);
