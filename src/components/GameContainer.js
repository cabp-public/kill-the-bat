import React, { Component } from 'react';
import { connect } from 'react-redux';
import KillTheBat from '../KillTheBat';

class GameContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const element = this.refs['layer-game'];

    KillTheBat.Game.setUp(element);
  }

  render() {
    const { PLAYER_DATA, GAME_PAUSED } = this.props;
    const className = 'layer layer-game' + (GAME_PAUSED ? ' paused' : '');

    return (
      <div ref="layer-game" className={ className } />
    )
  }
}

const mapStateToProps = (state) => KillTheBat.StoreManager.mapStateToProps('GameContainer', state);
export default connect(mapStateToProps, {})(GameContainer);
