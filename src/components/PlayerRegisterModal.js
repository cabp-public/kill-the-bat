import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as M from 'materialize-css';
import KillTheBat from './../KillTheBat';

class PlayerRegisterModal extends Component {
  constructor(props) {
    super(props);

    this.model = KillTheBat['PlayerModel'];

    const { PLAYER_DATA } = props;
    const playerData = PLAYER_DATA || this.model.getDefaults('PLAYER_DATA');
    this.state = { open: false, data: playerData };

    // Bindings
    this.onModalClose = this.onModalClose.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    const element = this.refs['modalForm'];
    const options = {
      dismissible: false,
      opacity: 0.4,
      onCloseEnd: this.onModalClose
    };

    this.modal = M.Modal.init(element, options);
  }

  componentWillReceiveProps(nextProps) {
    const { LAUNCH_MODAL_PLAYER_REGISTER } = nextProps;
    const { open } = this.state;

    if ( LAUNCH_MODAL_PLAYER_REGISTER && !open ) {
      this.modal.open();
      this.setState({ open: true });
    }

    if ( !LAUNCH_MODAL_PLAYER_REGISTER && open ) {
      this.modal.close();
      this.setState({ open: false });
    }
  }

  onModalClose() {
    KillTheBat.Game.pause(false);
  }

  onChange(event) {
    let { data } = this.state;
    const target = event.target;
    const key = target.getAttribute('id');

    data[key] = target.value.length ? target.value : false;

    this.setState({ data });
  }

  onSubmit(event) {
    const { data } = this.state;
    const canSubmit = this.model.validate(data);

    canSubmit && this.model.register(data);

    event.preventDefault();
  }

  render() {
    const { data } = this.state;

    return (
      <div ref="modalForm" className="modal grey-text">
        <form onSubmit={this.onSubmit} className="modal-content">
          <h5>New Player</h5>

          <div className="row">
            <div className="input-field col s6">
              <input type="text" id="name" defaultValue={ data.name || '' } onChange={this.onChange} autoComplete="off" className="grey-text validate" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="input-field col s6">
              <input type="email" id="email" defaultValue={ data.email || '' } onChange={this.onChange} autoComplete="off" className="grey-text validate" />
              <label htmlFor="email">e-mail</label>
            </div>
          </div>
          <div className="row center-align">
            <button type="submit" className="btn btn-small black transparent-half amber-text text-darken-3">Register</button>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => KillTheBat.StoreManager.mapStateToProps('PlayerRegisterModal', state);
export default connect(mapStateToProps, {})(PlayerRegisterModal);
