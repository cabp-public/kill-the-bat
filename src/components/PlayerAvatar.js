import React, { Component } from 'react';
import { connect } from 'react-redux';
import KillTheBat from './../KillTheBat';

class PlayerAvatar extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    const { PLAYER_DATA } = this.props;

    return (
      <div className="layer layer-avatar center-align">
        <button type="button" className="btn btn-small transparent-half center-block">
          <small className="grey-text">{ PLAYER_DATA.name }</small>
        </button>
      </div>
    )
  }
}

const mapStateToProps = (state) => KillTheBat.StoreManager.mapStateToProps('PlayerAvatar', state);
export default connect(mapStateToProps, {})(PlayerAvatar);
