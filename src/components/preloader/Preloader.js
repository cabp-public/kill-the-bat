import React, {Component} from 'react';
import { connect } from 'react-redux';
import Spinner from './Spinner';
import KillTheBat from './../../KillTheBat';

import siteLogo from '../../assets/img/logo.png';

class Preloader extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    const { LOADER_CURRENT_LOAD } = this.props;
    const { label } = LOADER_CURRENT_LOAD;

    return (
      <div className="layer layer-welcome">
        <div className="container">
          <div className="row">
            <div className="col s12 center-align">
              <img src={siteLogo} />

              <h4 className="font-anton text-shadow-default grey-text text-lighten-3">THERE'S A FLYING RAT IN TOWN!</h4>
              <p className="text-shadow-default">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>

              <Spinner />

              <p className="text-shadow-default">{ label }</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => KillTheBat.StoreManager.mapStateToProps('Preloader', state);
export default connect(mapStateToProps, {})(Preloader);
