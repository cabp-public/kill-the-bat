import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import KillTheBat from './KillTheBat';

import './assets/css/materialize.min.css';
import './assets/css/kill-the-bat.css';

const autoLoadOptions = {};

document.addEventListener(KillTheBat.eventNameAutoload, function (event) {
  render(
    <Provider store={KillTheBat.StoreManager.reduxStore}>
      <App />
    </Provider>,
    document.getElementById('index')
  );
});

KillTheBat.AutoLoad(autoLoadOptions);

/*
document.addEventListener(DataViewer.eventNameAutoload, function (event) {
  render(
    <Provider store={DataViewer.StoreManager.reduxStore}>
      <AppWrapper />
    </Provider>,
    document.getElementById('root')
  );
});
*/