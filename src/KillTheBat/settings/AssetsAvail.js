const AssetsAvail = [
  { name: 'background', label: 'Gotham City', url: 'gotham.jpg' },
  { name: 'card_batman', label: 'Characters', url: 'card_batman.png', hero: true },
  { name: 'enemy_joker', label: 'Characters', url: 'card_joker.png', hero: false },
  { name: 'enemy_catwoman', label: 'Characters', url: 'card_catwoman.png', hero: false },
  { name: 'enemy_riddler', label: 'Characters', url: 'card_riddler.png', hero: false },
  { name: 'enemy_harvey', label: 'Characters', url: 'card_harvey.png', hero: false },
  { name: 'knob_bet', label: 'Controls', url: 'knob_bet.png', hero: false },
  { name: 'knob_go', label: 'Controls', url: 'knob_go.png', hero: false },
  { name: 'control_bet_plus', label: 'Controls', url: 'control_bet_plus.png', hero: false },
  { name: 'control_bet_minus', label: 'Controls', url: 'control_bet_minus.png', hero: false },
  { name: 'control_bet_triangle', label: 'Controls', url: 'control_bet_triangle.png', hero: false },
  { name: 'control_top_right', label: 'Controls', url: 'control_top_right.png', hero: false },
  { name: 'control_bottom_left', label: 'Controls', url: 'control_bottom_left.png', hero: false },
  { name: 'control_play', label: 'Controls', url: 'control_play.png', hero: false },
  { name: 'balance', label: 'Controls', url: 'balance.png', hero: false },
  { name: 'musicMain', label: 'Music', url: 'partyman.{ogg,mp3}' }
];

export default AssetsAvail;