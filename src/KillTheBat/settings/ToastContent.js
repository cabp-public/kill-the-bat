const ToastContent = {
  error: '<i class="material-icons">notification_important</i>&nbsp;<small>[text]</small>',
  warning: '<i class="material-icons">warning</i>&nbsp;<small>[text]</small>',
  feedback: '<i class="material-icons">feedback</i>&nbsp;<small>[text]</small>'
};

export default ToastContent;