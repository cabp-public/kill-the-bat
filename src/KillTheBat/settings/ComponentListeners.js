export const ComponentListeners = {
  App: ['LOADER_ACTIVE'],
  Preloader: ['LOADER_CURRENT_LOAD'],
  Content: ['LOADER_ACTIVE', 'PLAYER_DATA', 'TOAST_VALUE'],
  GameContainer: ['PLAYER_DATA', 'GAME_PAUSED'],
  PlayerRegisterModal: ['LAUNCH_MODAL_PLAYER_REGISTER', 'PLAYER_DATA'],
  PlayerAvatar: ['PLAYER_DATA']
};