/*!
 * KillTheBat v0.1.0-beta
 * Carlos Bucheli Padilla
 */

import Config from './classes/Config';
import StoreManager from './classes/StoreManager';
import Game from './classes/Game';
import AssetLoader from './classes/AssetLoader';
import AudioManager from './classes/AudioManager';
import StageManager from './classes/StageManager';

import PlayerModel from './models/PlayerModel';
// import PlayerRegisterController from './controllers/PlayerRegisterController';

// Required for Meteor package, the use of window prevents export by Meteor
(function (window) {
  if (window.Package) {
    KillTheBat = {};
  } else {
    window.KillTheBat = {};
  }
})(window);

// AMD
if (typeof define === "function" && define.amd) {
  define("KillTheBat", [], function () {
    return KillTheBat;
  });
}
else if (typeof exports !== 'undefined' && !exports.nodeType) {
  if (typeof module !== 'undefined' && !module.nodeType && module.exports) {
    exports = module.exports = KillTheBat;
  }

  exports.default = KillTheBat;
}

// Autoload Classes
KillTheBat.eventNameAutoload = 'AUTOLOAD_READY';

KillTheBat.AutoLoad = function (options) {
  // console.clear();
  console.log('AUTOLOADING...');

  const eventAutoloadReady = new CustomEvent(KillTheBat.eventNameAutoload);

  this.pixiApp = false;
  this.initialState = {};
  const self = this;

  // Classes & Models
  const registry = {
    Config: Config(this, options.Config),
    AssetLoader: AssetLoader(this, options.AssetLoader || {}),
    AudioManager: AudioManager(this, options.AudioManager || {}),
    Game: Game(this, options.Game || {}),
    PlayerModel: PlayerModel(this, options.PlayerModel || {}),
    StageManager: StageManager(this, options.StageManager || {}),
    StoreManager: StoreManager(this, options.StoreManager || {})
  };

  const registryKeys = Object.keys(registry);

  registryKeys.forEach(function (key) {
    self[key] = registry[key];
  });

  // Controllers
  // const controllersAvail = {
  //   PlayerRegisterController: PlayerRegisterController(this, {})
  // };

  // const controllersKeys = Object.keys(controllersAvail);
  // self['Controllers'] = {};

  // controllersKeys.forEach(function (key) {
  //   self['Controllers'][key] = controllersAvail[key];
  // });

  // console.log('______________________________');
  // console.log('INITIAL STATE');
  // console.log(this.initialState);
  // console.log('______________________________');

  document.dispatchEvent(eventAutoloadReady);
};

/**
 * Sets the .env variables
 * @param {string} hash  String returned from this.hash
 * @returns {string}
 */
KillTheBat.escapeHash = function (hash) {
  return hash.replace(/(:|\.|\[|\]|,|=|\/)/g, "\\$1");
};



