const PlayerModel = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {
    STORE_KEYS: {
      playerData: 'PLAYER_DATA',
      launchModalPlayerRegister: 'LAUNCH_MODAL_PLAYER_REGISTER'
    },
    STORAGE_KEY: 'KILL_BAT__',
    DEFAULTS: {
      PLAYER_DATA: { name: false, email: false, score: { match: 0, global: 0 } }
    }
  };

  /**
   * @class
   */
  class PlayerModel {
    /**
     * Construct PlayerModel instance and set up
     * @constructor
     */
    constructor(options) {
      // Initial State
      const storeKey = _defaults.STORAGE_KEY + _defaults.STORE_KEYS.playerData;
      const storedData = localStorage.getItem(storeKey) || false;

      KillTheBat.initialState[_defaults.STORE_KEYS.playerData] = storedData && JSON.parse(storedData);
      KillTheBat.initialState[_defaults.STORE_KEYS.launchModalPlayerRegister] = false;

      // Bindings
      this.getDefaults = this.getDefaults.bind(this);
      this.validate = this.validate.bind(this);
      this.register = this.register.bind(this);
    }

    /**
     * Returns default data for the given key, if any
     * @param key
     * @returns {DEFAULTS|{PLAYER_DATA}}
     */
    getDefaults(key = false) {
      return key ? _defaults.DEFAULTS[key] : _defaults.DEFAULTS;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.PlayerModel = undefined;
    }

    /**
     * Validate player data
     * @param data
     * @returns {boolean}
     */
    validate(data) {
      const { name, email } = data;
      const validBool = name !== false && email !== false;
      const validEmail = validBool && email.indexOf('@') > -1 && email.indexOf('.') > -1;

      return validBool && validEmail;
    }

    /**
     * Register new player
     * @param data
     */
    register(data) {
      const actions = [];
      const storeKey = _defaults.STORAGE_KEY + _defaults.STORE_KEYS.playerData;

      localStorage.setItem(storeKey, JSON.stringify(data));

      actions.push({
        type: _defaults.STORE_KEYS.playerData,
        value: data
      });

      actions.push({
        type: _defaults.STORE_KEYS.launchModalPlayerRegister,
        value: false
      });

      KillTheBat.StoreManager.setKeys(actions);
      KillTheBat.AudioManager.stop('musicMain', true);
    }
  }

  KillTheBat.PlayerModel = PlayerModel.getInstance(constructOptions);

  return PlayerModel.getInstance(constructOptions);

});

export default PlayerModel;
