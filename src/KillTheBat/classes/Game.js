import * as PIXI from 'pixi.js';
import ToastContent from './../settings/ToastContent';

const Game = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {
    STORE_KEYS: {
      loader: 'LOADER_ACTIVE',
      loaderCurrentLoad: 'LOADER_CURRENT_LOAD',
      launchModalPlayerRegister: 'LAUNCH_MODAL_PLAYER_REGISTER',
      playerData: 'PLAYER_DATA',
      currentBet: 'CURRENT_BET',
      currentBalance: 'CURRENT_BALANCE',
      selectedEnemy: 'SELECTED_ENEMY',
      toastValue: 'TOAST_VALUE',
      gamePaused: 'GAME_PAUSED'
    }
  };

  /**
   * @class
   */
  class Game {
    /**
     * Construct Game instance and set up
     * @constructor
     */
    constructor(options) {
      this.wrapperElement = null;

      // PIXI params
      this.gameParams = {
        width: window.innerWidth,
        height: window.innerHeight,
        antialias: true,
        transparent: true,
        resolution: 1,
      };

      // Initial State
      KillTheBat.initialState[_defaults.STORE_KEYS.gamePaused] = true;
      KillTheBat.initialState[_defaults.STORE_KEYS.currentBet] = 1;
      KillTheBat.initialState[_defaults.STORE_KEYS.currentBalance] = 1000;

      // Bindings
      // this.setEventListener = this.setEventListener.bind(this);
      this.setUp = this.setUp.bind(this);
      this.onAllAssetsLoaded = this.onAllAssetsLoaded.bind(this);
      this.resetBet = this.resetBet.bind(this);
      this.increaseBet = this.increaseBet.bind(this);
      this.decreaseBet = this.decreaseBet.bind(this);
      this.start = this.start.bind(this);
      this.pause = this.pause.bind(this);

      // Events
      this.eventListeners = {
        eAllAssetsLoaded: this.onAllAssetsLoaded
      };
    }

    static get defaults() {
      return _defaults;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.Game = undefined;
    }

    /**
     * Setup a new game
     * @param element
     */
    setUp(element) {
      const eventKey = 'eAllAssetsLoaded';

      this.wrapperElement = element;
      KillTheBat.pixiApp = new PIXI.Application(this.gameParams);

      element.addEventListener(eventKey, this.eventListeners[eventKey]);
      element.appendChild(KillTheBat.pixiApp.view);

      KillTheBat.AssetLoader.start(element);
    }

    /**
     * Event listener triggered when ALL assets were successfully loaded
     * @param event
     */
    onAllAssetsLoaded(event) {
      const actions = [];
      const playerData = KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.playerData);

      KillTheBat.AudioManager.play('musicMain');

      actions.push({
        type: _defaults.STORE_KEYS.loader,
        value: false
      });

      actions.push({
        type: _defaults.STORE_KEYS.gamePaused,
        value: playerData === false
      });

      playerData === false && actions.push({
        type: _defaults.STORE_KEYS.launchModalPlayerRegister,
        value: true
      });

      KillTheBat.StoreManager.setKeys(actions);
      KillTheBat.StageManager.setUp();
    }

    resetBet() {}

    increaseBet(by = 1) {
      let value = parseInt(KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.currentBet));

      if ( value < 5 ) {
        value += by;

        KillTheBat.StoreManager.setKey({
          type: _defaults.STORE_KEYS.currentBet,
          value
        });

        KillTheBat.StageManager.updateBet(true);
      }
    }

    decreaseBet(by = 1) {
      let value = parseInt(KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.currentBet));

      if ( value > 1 ) {
        value -= by;

        KillTheBat.StoreManager.setKey({
          type: _defaults.STORE_KEYS.currentBet,
          value
        });

        KillTheBat.StageManager.updateBet();
      }
    }

    start() {
      // const { currentBet, currentBalance } = _defaults.STORE_KEYS;
      const enemy = KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.selectedEnemy);
      const canStart = enemy || false;
      const errorMessage = 'Select an enemy to start';
      const style = 'error';
      const toastContent = ToastContent[style].replace('[text]', errorMessage);

      let action = {
        type: _defaults.STORE_KEYS.toastValue,
        value: false
      };

      if ( canStart ) {
        action.value = false;
        console.log('start!');
      }
      else {
        action.value = {
          style,
          html: toastContent
        };

        KillTheBat.StoreManager.setKey(action);
      }
    }

    pause(value = true) {
      KillTheBat.StoreManager.setKey({
        type: _defaults.STORE_KEYS.gamePaused,
        value: value
      });
    }
  }

  return Game.getInstance(constructOptions);

});

export default Game;
