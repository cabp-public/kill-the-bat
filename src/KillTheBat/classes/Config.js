const Config = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {
    STORE_KEYS: {
      toastValue: 'TOAST_VALUE',
      audioOn: 'AUDIO_ON'
    }
  };

  /**
   * @class
   */
  class Config {
    /**
     * Construct Config instance and set up
     * @constructor
     */
    constructor(options) {
      // Initial State
      KillTheBat.initialState[_defaults.STORE_KEYS.toastValue] = false;
      KillTheBat.initialState[_defaults.STORE_KEYS.audioOn] = true;
    }

    static get defaults() {
      return _defaults;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.Config = undefined;
    }
  }

  KillTheBat.Config = Config.getInstance(constructOptions);

  return Config.getInstance(constructOptions);

});

export default Config;
