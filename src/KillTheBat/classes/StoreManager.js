import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { ComponentListeners } from './../settings/ComponentListeners';

const StoreManager = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {};

  /**
   * @class
   */
  class StoreManager {
    /**
     * Construct StoreManager instance and set up
     * @constructor
     */
    constructor(options) {
      _defaults = KillTheBat.initialState;

      const storeKeys = Object.keys(_defaults);
      const reducers = {};

      storeKeys.forEach(function (key) {
        reducers[key] = function (state = _defaults[key], action) {
          switch (action.type) {
            case key:
              return action.value;

            default:
              return state;
          }
        };
      });

      this.reduxStore = createStore(combineReducers(reducers), {}, applyMiddleware(thunk));

      // Bindings
      this.getKey = this.getKey.bind(this);
      this.setKey = this.setKey.bind(this);
      this.setKeys = this.setKeys.bind(this);
      this.mapStateToProps = this.mapStateToProps.bind(this);
    }

    static get defaults() {
      return _defaults;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.StoreManager = undefined;
    }

    getKey(key) {
      const state = this.reduxStore.getState();
      return state[key] || false;
    }

    /**
     * Update Single Key
     */
    setKey(action) {
      this.reduxStore.dispatch(action);
    }

    /**
     * Set several keys from the given array of actions
     * @param params
     */
    setKeys(params) {
      const self = this;

      params.forEach(function (action) {
        self.setKey(action);
      })
    }

    /**
     * Returns the key-value pairs from the state to a component
     * @param componentName
     * @param state
     * @returns {{}}
     */
    mapStateToProps(componentName, state) {
      const keys = ComponentListeners[componentName];
      const values = {};

      keys.forEach(function (key) {
        values[key] = state[key];
      });

      return values;
    }
  }

  KillTheBat.StoreManager = StoreManager.getInstance(constructOptions);

  return KillTheBat.StoreManager;

});

export default StoreManager;
