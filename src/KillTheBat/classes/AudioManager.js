import * as PIXI from 'pixi.js';
import * as PIXI_SOUND from 'pixi-sound';

const AudioManager = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {
    STORE_KEYS: {
      audioOn: 'AUDIO_ON'
    },
    ASSETS_DIR: 'assets/',
    VOLUME_FADE_CURRENT_VALUE: 1.0
  };

  /**
   * @class
   */
  class AudioManager {
    /**
     * Construct AudioManager instance and set up
     * @constructor
     */
    constructor(options) {
      this.currentSound = false;
      this.stopFadeInterval = false;

      // Bindings
      this.play = this.play.bind(this);
      this.stop = this.stop.bind(this);
      this.volumeReduce = this.volumeReduce.bind(this);
      this.toggleOnOff = this.toggleOnOff.bind(this);
    }

    static get defaults() {
      return _defaults;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.AudioManager = undefined;
    }

    /**
     * Play the requested sound resource
     * @param name
     */
    play(name) {
      const sound = PIXI.loader.resources[name].sound || false;

      if ( sound ) {
        sound.volume = 1;
        sound.play();
      }
    }

    /**
     * Stop the requested sound resource, optionally fading-in its volume
     * @param name
     * @param fade
     */
    stop(name, fade = false) {
      const self = this;
      const sound = PIXI.loader.resources[name].sound || false;
      this.currentSound = sound;

      if ( fade ) {
        this.stopFadeInterval = setInterval(function () {
          self.volumeReduce(false, true);
        }, 200);
      }
      else {
        sound && sound.stop();
      }
    }

    /**
     * Reduce volume for current or requested sound
     * @param resource
     * @param stopAtZero
     * @param by
     */
    volumeReduce(resource = false, stopAtZero = false, by = 0.1) {
      const sound = this.currentSound || resource;
      const volCurrent = sound.volume;

      if ( volCurrent > 0 ) {
        this.currentSound.volume -= by;
      }
      else if ( stopAtZero && volCurrent <= 0 ) {
        this.currentSound.stop();

        if ( this.stopFadeInterval ) {
          clearInterval(this.stopFadeInterval);

          this.currentSound = false;
          this.stopFadeInterval = false;
        }
      }
    }

    toggleOnOff(audioName) {
      const audioOn = KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.audioOn);

      if ( audioOn ) {
        KillTheBat.StageManager.assets['iconSound'].text = 'volume_up';
        this.stop(audioName, true);

        KillTheBat.StoreManager.setKey({
          type: _defaults.STORE_KEYS.audioOn,
          value: false
        });
      }
      else {
        KillTheBat.StageManager.assets['iconSound'].text = 'volume_off';
        this.play(audioName);

        KillTheBat.StoreManager.setKey({
          type: _defaults.STORE_KEYS.audioOn,
          value: true
        });
      }
    }
  }

  return AudioManager.getInstance(constructOptions);

});

export default AudioManager;
