import * as PIXI from 'pixi.js';
import AssetsAvail from './../settings/AssetsAvail';

const AssetLoader = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {
    STORE_KEYS: {
      loader: 'LOADER_ACTIVE',
      loaderCurrentLoad: 'LOADER_CURRENT_LOAD'
    },
    ASSETS_DIR: 'assets/'
  };

  /**
   * @class
   */
  class AssetLoader {
    /**
     * Construct AssetLoader instance and set up
     * @constructor
     */
    constructor(options) {
      this.currentAssetIndex = 0;

      // Initial State
      KillTheBat.initialState[_defaults.STORE_KEYS.loader] = true;
      KillTheBat.initialState[_defaults.STORE_KEYS.loaderCurrentLoad] = { label: 'Loading...', progress: 0 };

      // Bindings
      this.start = this.start.bind(this);
      this.loadSingleAsset = this.loadSingleAsset.bind(this);
      this.onProgress = this.onProgress.bind(this);
      this.onAssetLoaded = this.onAssetLoaded.bind(this);
      this.onAllAssetsLoaded = this.onAllAssetsLoaded.bind(this);

      // Events
      this.events = {
        eAllAssetsLoaded: new CustomEvent('eAllAssetsLoaded')
      };
    }

    static get defaults() {
      return _defaults;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.AssetLoader = undefined;
    }

    start(element) {
      this.wrapperElement = element;
      this.loadSingleAsset();
    }

    loadSingleAsset() {
      const asset = AssetsAvail[this.currentAssetIndex];
      const output = { label: asset.label, progress: 0 };
      const action = {
        type: _defaults.STORE_KEYS.loaderCurrentLoad,
        value: output
      };

      KillTheBat.StoreManager.setKey(action);

      PIXI.loader
        .add(asset.name, _defaults.ASSETS_DIR + asset.url)
        .on('progress', this.onProgress)
        .load(this.onAssetLoaded);
    }

    onProgress(loader, resource) {
      const asset = AssetsAvail[this.currentAssetIndex];
      const output = { label: asset.label, progress: loader.progress };
      const action = {
        type: _defaults.STORE_KEYS.loaderCurrentLoad,
        value: output
      };

      KillTheBat.StoreManager.setKey(action);
    }

    onAssetLoaded(loader, resource) {
      const self = this;
      const index = this.currentAssetIndex;
      const indexMax = AssetsAvail.length - 1;

      if ( index >= indexMax ) {
        this.onAllAssetsLoaded();
      }
      else if ( index < AssetsAvail.length - 1 ) {
        this.currentAssetIndex++;

        setTimeout(function () {
          self.loadSingleAsset();
        }, 250)
      }
    }

    onAllAssetsLoaded() {
      const self = this;
      const eventKey = 'eAllAssetsLoaded';

      KillTheBat.StoreManager.setKey({
        type: _defaults.STORE_KEYS.loaderCurrentLoad,
        value: { label: 'COMPLETE', progress: 100 }
      });

      setTimeout(function () {
        self.wrapperElement.dispatchEvent(self.events[eventKey]);
      }, 1000);
    }
  }

  return AssetLoader.getInstance(constructOptions);

});

export default AssetLoader;
