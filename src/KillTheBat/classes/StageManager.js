import * as PIXI from 'pixi.js';
import AssetsAvail from './../settings/AssetsAvail';

const StageManager = (function (KillTheBat, constructOptions) {
  'use strict';

  let instance, _defaults = {
    STORE_KEYS: {
      currentBet: 'CURRENT_BET',
      audioOn: 'AUDIO_ON',
      currentBalance: 'CURRENT_BALANCE',
    },
    scales: { hero: 0.6, default: 0.4 },
    measures: {},
    betIndicatorValues: {
      1:	0,
      2:	-0.8,
      3:	-1.6,
      4:	-2.4,
      5:	-3.2
    },
    textStyles: {
      TrajanPro : {
        fontFamily: 'Trajan Pro',
        fontSize: 44,
        fill: 'white',
        strokeThickness: 1
      },
      MaterialIcons : {
        fontFamily: 'Material Icons',
        fontSize: 32,
        fontWeight: 'normal',
        fontStyle: 'normal',
        fill: 'rgba(255, 255, 255, 0)',
        stroke: 'rgba(0, 0, 0, 1)',
        strokeThickness: 1.5,
        dropShadow: true,
        dropShadowColor: 'white',
        dropShadowBlur: 5,
        dropShadowDistance: 0
      }
    }
  };

  /**
   * @class
   */
  class StageManager {
    /**
     * Construct StageManager instance and set up
     * @constructor
     */
    constructor(options) {
      this.resources = { cards: [] };
      this.assets = {};
      this.tickers = {};

      // Bindings
      this.setUp = this.setUp.bind(this);
      this.randomizeCards = this.randomizeCards.bind(this);
      this.addAssetsToStage = this.addAssetsToStage.bind(this);
      this.addBetControlsToStage = this.addBetControlsToStage.bind(this);
      this.addGoControlToStage = this.addGoControlToStage.bind(this);
      this.addLabelBalance = this.addLabelBalance.bind(this);
      this.setTickers = this.setTickers.bind(this);
      this.betIndicatorTickHandler = this.betIndicatorTickHandler.bind(this);
      this.updateBet = this.updateBet.bind(this);
      this.selectCard = this.selectCard.bind(this);
    }

    static get defaults() {
      return _defaults;
    }

    /**
     * Get Instance
     */
    static getInstance(constructOptions) {
      if (!instance) {
        instance = new this(constructOptions);
      }

      return instance;
    }

    /**
     * Destroy Instance
     */
    destroy() {
      KillTheBat.StageManager = undefined;
    }

    setUp() {
      const maxX = KillTheBat.pixiApp.renderer.width;
      const maxY = KillTheBat.pixiApp.renderer.height;
      let cardHero, randomCards, cards = [];

      AssetsAvail.forEach(function (definition) {
        const isHero = definition.hero || false;

        if ( definition.name.indexOf('enemy_') === 0 && !isHero ) {
          cards.push(PIXI.loader.resources[definition.name]);
        }

        if ( definition.hero ) {
          cardHero = PIXI.loader.resources[definition.name];
        }
      });

      randomCards = this.randomizeCards(cards);
      // randomCards.splice(2, 0, cardHero);

      this.resources.cards = randomCards;

      _defaults.measures = {
        maxX: maxX,
        maxY: maxY,
        centerX: maxX / 2,
        centerY: maxY / 2
      };

      this.addAssetsToStage(cardHero);
    }

    randomizeCards(cards) {
      let randomized = [];
      let randomizedCards = [];
      let pos;

      do {
        pos = Math.floor(Math.random() * cards.length);

        if ( randomized.indexOf(pos) === -1 ) {
          randomized.push(pos);
        }
      }
      while (randomized.length < cards.length);

      randomized.forEach(function (position) {
        randomizedCards.push(cards[position]);
      });

      return randomizedCards;
    }

    addAssetsToStage(cardHero) {
      const self = this;
      const cardsAvail = this.resources.cards;

      let cardSprite, cardIndex = 0;
      // let startX = _defaults.measures.centerX;

      // Hero
      cardSprite = new PIXI.Sprite(cardHero.texture);
      cardSprite.scale.x *= _defaults.scales.hero;
      cardSprite.scale.y *= _defaults.scales.hero;
      cardSprite.x = _defaults.measures.centerX;
      cardSprite.y = _defaults.measures.centerY;
      cardSprite.interactive = true;
      cardSprite.buttonMode = true;
      cardSprite.anchor.set(0.5);

      this.assets['hero'] = cardSprite;
      KillTheBat.pixiApp.stage.addChild(cardSprite);

      // Enemies
      const enemyWidth = (_defaults.scales.default * cardSprite.width) / _defaults.scales.hero;
      const halfIndex = Math.floor((cardsAvail.length - 1) / 2);
      const marginRight = 10;
      const centerGap = enemyWidth * 3;

      let enemies = {};
      let posX = _defaults.measures.centerX - (enemyWidth * ((cardsAvail.length - 1) / 2)) - enemyWidth;
      posX -= marginRight * 1.5;

      cardsAvail.forEach(function (resource) {
        let cardSprite = new PIXI.Sprite(resource.texture);

        cardSprite.scale.x *= _defaults.scales.default;
        cardSprite.scale.y *= _defaults.scales.default;
        cardSprite.x = posX;
        cardSprite.y = _defaults.measures.centerY;
        cardSprite.name = resource.name;
        cardSprite.index = cardIndex;
        cardSprite.interactive = true;
        cardSprite.buttonMode = true;
        cardSprite.anchor.set(0.5);
        cardSprite.on('pointerdown', self.selectCard);
        cardSprite.on('touchend', (e) => {
          self.selectCard(e);
        });

        enemies[cardSprite.name] = cardSprite;
        KillTheBat.pixiApp.stage.addChild(cardSprite);

        posX += cardIndex === halfIndex ? centerGap : cardSprite.width;
        posX += marginRight;

        cardIndex++;
      });

      this.assets['enemies'] = enemies;
      this.addBetControlsToStage();
    }

    addBetControlsToStage() {
      // Controls: Bet
      const knobBet = PIXI.loader.resources['knob_bet'];
      const resourceControlBetPlus = PIXI.loader.resources['control_bet_plus'];
      const resourceControlBetMinus = PIXI.loader.resources['control_bet_minus'];
      const resourceControlBetIndicator = PIXI.loader.resources['control_bet_triangle'];
      const knobSprite = new PIXI.Sprite(knobBet.texture);
      const spriteControlBetPlus = new PIXI.Sprite(resourceControlBetPlus.texture);
      const spriteControlBetMinus = new PIXI.Sprite(resourceControlBetMinus.texture);
      const spriteControlBetIndicator = new PIXI.Sprite(resourceControlBetIndicator.texture);

      // Background
      knobSprite.scale.x *= _defaults.scales.hero;
      knobSprite.scale.y *= _defaults.scales.hero;
      knobSprite.x = (knobSprite.width / 2) + 5;
      knobSprite.y = _defaults.measures.maxY - knobSprite.height / 1.8;

      knobSprite.interactive = false;
      knobSprite.buttonMode = false;
      knobSprite.anchor.set(0.5);

      KillTheBat.pixiApp.stage.addChild(knobSprite);

      // Indicator
      spriteControlBetIndicator.scale.x *= _defaults.scales.hero;
      spriteControlBetIndicator.scale.y *= _defaults.scales.hero;
      spriteControlBetIndicator.anchor.set(0.5);
      spriteControlBetIndicator.x = knobSprite.x;
      spriteControlBetIndicator.y = knobSprite.y;
      spriteControlBetIndicator.interactive = false;
      spriteControlBetIndicator.buttonMode = false;

      this.assets['betIndicator'] = spriteControlBetIndicator;
      KillTheBat.pixiApp.stage.addChild(spriteControlBetIndicator);

      // Text
      const defaultText = KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.currentBet);
      const style = new PIXI.TextStyle(_defaults.textStyles.TrajanPro);
      const textBet = new PIXI.Text(defaultText, style);

      textBet.anchor.set(0.5);
      textBet.x = knobSprite.x - 2;
      textBet.y = knobSprite.y;

      this.assets['textBet'] = textBet;
      KillTheBat.pixiApp.stage.addChild(textBet);

      // Control Plus
      spriteControlBetPlus.scale.x *= _defaults.scales.hero;
      spriteControlBetPlus.scale.y *= _defaults.scales.hero;
      spriteControlBetPlus.x = knobSprite.x - 1;
      spriteControlBetPlus.y = knobSprite.y - (spriteControlBetPlus.height / 2) + 5;
      spriteControlBetPlus.interactive = true;
      spriteControlBetPlus.buttonMode = true;
      spriteControlBetPlus.anchor.set(0.5);
      spriteControlBetPlus.click = (e) => { KillTheBat.Game['increaseBet']() };
      spriteControlBetPlus.on('touchend', function(e) {
        KillTheBat.Game['increaseBet']();
      });

      this.assets['controlBetPlus'] = spriteControlBetPlus;
      KillTheBat.pixiApp.stage.addChild(spriteControlBetPlus);

      // Control Minus
      spriteControlBetMinus.scale.x *= _defaults.scales.hero;
      spriteControlBetMinus.scale.y *= _defaults.scales.hero;
      spriteControlBetMinus.x = spriteControlBetPlus.x;
      spriteControlBetMinus.y = spriteControlBetPlus.y + spriteControlBetMinus.height - 7;
      spriteControlBetMinus.interactive = true;
      spriteControlBetMinus.buttonMode = true;
      spriteControlBetMinus.anchor.set(0.5);
      spriteControlBetMinus.click = (e) => { KillTheBat.Game['decreaseBet']() };
      spriteControlBetMinus.on('touchend', function(e) {
        KillTheBat.Game['decreaseBet']();
      });

      this.assets['controlBetMinus'] = spriteControlBetMinus;
      KillTheBat.pixiApp.stage.addChild(spriteControlBetMinus);

      this.addGoControlToStage();
    }

    addGoControlToStage() {
      const knobGo = PIXI.loader.resources['knob_go'];
      const resourceControlSettings = PIXI.loader.resources['control_top_right'];
      const resourceControlSound = PIXI.loader.resources['control_bottom_left'];
      const resourceControlPlay = PIXI.loader.resources['control_play'];
      const knobSprite = new PIXI.Sprite(knobGo.texture);
      const spriteControlSettings = new PIXI.Sprite(resourceControlSettings.texture);
      const spriteControlSound = new PIXI.Sprite(resourceControlSound.texture);
      const spriteControlPlay = new PIXI.Sprite(resourceControlPlay.texture);

      const styleText = new PIXI.TextStyle(_defaults.textStyles.MaterialIcons);
      const iconSettings = new PIXI.Text('settings', styleText);
      const iconSound = new PIXI.Text('volume_off', styleText);

      // Background
      knobSprite.scale.x *= _defaults.scales.hero;
      knobSprite.scale.y *= _defaults.scales.hero;
      knobSprite.x = _defaults.measures.maxX - (knobSprite.width / 2) - 5;
      knobSprite.y = _defaults.measures.maxY - knobSprite.height / 1.8;
      knobSprite.interactive = false;
      knobSprite.buttonMode = false;
      knobSprite.anchor.set(0.5);

      KillTheBat.pixiApp.stage.addChild(knobSprite);

      // Control Settings
      spriteControlSettings.scale.x *= _defaults.scales.hero;
      spriteControlSettings.scale.y *= _defaults.scales.hero;
      spriteControlSettings.x = knobSprite.x + (spriteControlSettings.width / 2) + 2;
      spriteControlSettings.y = knobSprite.y - (spriteControlSettings.height / 2) - 1;
      spriteControlSettings.interactive = true;
      spriteControlSettings.buttonMode = true;
      spriteControlSettings.anchor.set(0.5);
      // spriteControlSettings.click = (e) => { KillTheBat.Game['increaseBet']() };
      // spriteControlSettings.on('touchend', function(e) {
      // });

      // Control Sound
      spriteControlSound.scale.x *= _defaults.scales.hero;
      spriteControlSound.scale.y *= _defaults.scales.hero;
      spriteControlSound.x = spriteControlSettings.x - spriteControlSound.width - 1;
      spriteControlSound.y = spriteControlSettings.y + spriteControlSound.height + 2;
      spriteControlSound.interactive = true;
      spriteControlSound.buttonMode = true;
      spriteControlSound.anchor.set(0.5);
      spriteControlSound.click = (e) => { KillTheBat.AudioManager['toggleOnOff']('musicMain') };
      spriteControlSound.on('touchend', function(e) {
        KillTheBat.AudioManager['toggleOnOff']('musicMain');
      });

      // Control Play
      spriteControlPlay.scale.x *= _defaults.scales.hero;
      spriteControlPlay.scale.y *= _defaults.scales.hero;
      spriteControlPlay.x = knobSprite.x;
      spriteControlPlay.y = knobSprite.y;
      spriteControlPlay.interactive = true;
      spriteControlPlay.buttonMode = true;
      spriteControlPlay.anchor.set(0.5);
      spriteControlPlay.click = (e) => { KillTheBat.Game['start']() };
      spriteControlPlay.on('touchend', function(e) {
        KillTheBat.Game['start']();
      });

      // Settings Icon
      iconSettings.anchor.set(0.5);
      iconSettings.x = spriteControlSettings.x;
      iconSettings.y = spriteControlSettings.y;
      iconSettings.text = '';
      iconSettings.text = 'settings';

      // Sound Icon
      iconSound.anchor.set(0.5);
      iconSound.x = spriteControlSound.x - 5;
      iconSound.y = spriteControlSound.y + 5;

      this.assets['controlSettings'] = spriteControlSettings;
      this.assets['controlSound'] = spriteControlSound;
      this.assets['iconSound'] = iconSound;

      KillTheBat.pixiApp.stage.addChild(iconSettings);
      KillTheBat.pixiApp.stage.addChild(iconSound);
      KillTheBat.pixiApp.stage.addChild(spriteControlSettings);
      KillTheBat.pixiApp.stage.addChild(spriteControlSound);
      KillTheBat.pixiApp.stage.addChild(spriteControlPlay);

      this.addLabelBalance();
    }

    addLabelBalance() {
      const currentBalance = KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.currentBalance);
      const resourceBackground = PIXI.loader.resources['balance'];
      const spriteBackground = new PIXI.Sprite(resourceBackground.texture);
      const styleText = new PIXI.TextStyle(_defaults.textStyles.TrajanPro);
      const textBalance = new PIXI.Text(currentBalance, styleText);

      // Background
      spriteBackground.anchor.set(0.5);
      spriteBackground.x = _defaults.measures.centerX;
      spriteBackground.y = 40;
      spriteBackground.interactive = false;
      spriteBackground.buttonMode = false;

      // Balance Text
      textBalance.anchor.set(0.5);
      textBalance.x = spriteBackground.x;
      textBalance.y = spriteBackground.y;

      KillTheBat.pixiApp.stage.addChild(textBalance);
      KillTheBat.pixiApp.stage.addChild(spriteBackground);

      this.setTickers();
    }

    setTickers() {
      const betIndicatorTicker = new PIXI.ticker.Ticker();

      // Bet Indicator
      betIndicatorTicker.autoStart = false;
      betIndicatorTicker.add(this.betIndicatorTickHandler);

      this.tickers['betIndicator'] = {
        positive: true,
        value: 1,
        ticker: betIndicatorTicker
      };
    }

    betIndicatorTickHandler(delta) {
      const betIndicator = this.assets['betIndicator'];
      const { positive, value, ticker } = this.tickers['betIndicator'];
      const maxValue = _defaults['betIndicatorValues'][value];

      if ( positive ) {
        betIndicator.rotation -= 0.1 * delta;
        betIndicator.rotation <= maxValue && ticker.stop();
      }
      else {
        betIndicator.rotation += 0.1 * delta;
        betIndicator.rotation >= maxValue && ticker.stop();
      }
    }

    updateBet(positive = false) {
      const value = parseInt(KillTheBat.StoreManager.getKey(_defaults.STORE_KEYS.currentBet));
      const textBet = this.assets['textBet'];
      const tickerKey = 'betIndicator';

      textBet.text = value;

      this.tickers[tickerKey].positive = positive;
      this.tickers[tickerKey].value = value;
      // this.tickers[tickerKey].ticker.stop();
      this.tickers[tickerKey].ticker.start();
    }

    selectCard(event) {
      const self = this;
      const card = event.target;
      const enemyKeys = Object.keys(this.assets['enemies']);

      // enemyKeys.forEach(function (key) {
      //   self.assets['enemies'][key].alpha = 0.75;
      // });

      // this.assets['enemies'][card.name].alpha = 1;
    }
  }

  return StageManager.getInstance(constructOptions);

});

export default StageManager;
